	import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.exception.StepFailedException
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
////Navigate to SDMC website and attempt to log in
//WebUI.openBrowser('')
//WebUI.navigateToUrl('http://sdmc-katalon.azurewebsites.net/')
//WebUI.setText(findTestObject('Page_SDMC/input_input-username'), 'KATALON5566')
//WebUI.setText(findTestObject('Page_SDMC/input_input-password'), 'KATALON5566')

////Capture username entered into username label
//String userName = WebUI.getAttribute(findTestObject('Page_SDMC/input_input-username'), "value")
//
//WebUI.click(findTestObject('Page_SDMC/button_Sign In'))

////Check if user is redirected to change password page
//WebUI.verifyElementPresent(findTestObject('Page_SDMC/changepass'), 5)



WebUI.callTestCase(findTestCase("Login to SDMC"), null)
//Input corrrect information into change password fields
WebUI.setText(findTestObject('Page_SDMC/input_input-CurrentPassword'), 'Saaaam')
WebUI.setText(findTestObject('Page_SDMC/input_input-NewPassword'), 'KATALON5567')
WebUI.setText(findTestObject('Page_SDMC/input_input-ConfirmPassword'), 'KATALON5567')

//Verify that the submit button is clickable, then submit
WebUI.verifyElementPresent(findTestObject('Page_SDMC/button_Submit'), 10)
WebUI.click(findTestObject('Page_SDMC/button_Submit'))

//Verify that the correct username is dispayed at the top right of the page after submitting
WebUI.verifyElementText(findTestObject("Page_SDMC/UsernameAfterLogin"), 'Diegoo')

