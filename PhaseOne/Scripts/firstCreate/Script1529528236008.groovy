import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://sds-katalon.azurewebsites.net/Account/Login?ReturnUrl=%2F')

//check for seeing the SDS website was launched
assert WebUI.getWindowTitle() == 'Login' : 'Could not reach SDS website'

// check for Synect logo
assert WebUI.verifyElementPresent(findTestObject('Page_Login_ForCreateUser/img_loginPage-logo'), 5) : 'Synect logo did not load'

// Check for password box
assert WebUI.verifyElementPresent(findTestObject('Page_Login_ForCreateUser/input_Password'), 5) : 'Password was not present'

// Check for email box
assert WebUI.verifyElementPresent(findTestObject('Page_Login_ForCreateUser/input_Email'), 5) : 'Input Email not shown'

// Check for login button
assert WebUI.verifyElementPresent(findTestObject('Page_Login_ForCreateUser/input_loginPage-button'), 5) : 'Login button not present'

WebUI.waitForElementVisible(findTestObject('Page_Login (1)/input_Email'), 3)

WebUI.waitForElementVisible(findTestObject('Page_Login (1)/input_Password'), 3)

//WebUI.doubleClick(findTestObject('Page_Login (1)/input_Email'))

WebUI.setText(findTestObject('Page_Login (1)/input_Email'), 'Admin@synectmedia.com')

WebUI.setText(findTestObject('Page_Login (1)/input_Password'), 'Admin@1234')

WebUI.sendKeys(findTestObject('Page_Login (1)/input_Password'), Keys.chord(Keys.ENTER))

WebUI.waitForElementVisible(findTestObject('Page_Synect Data Server (1)/a_Dev Tools'), 3)

//Check to see if you are logged in
assert WebUI.verifyElementPresent(findTestObject('Page_Synect Data Server_CreateUser/a_adminsynectmedia.com'), 5) : 'Log in email is not shown'

assert WebUI.verifyElementPresent(findTestObject('Page_Synect Data Server (2)/a_Dev Tools'), 5) : 'Dev tools not shown'

WebUI.waitForElementVisible(findTestObject('Page_Synect Data Server (1)/a_Dev Tools'), 3)

WebUI.click(findTestObject('Page_Synect Data Server (1)/a_Dev Tools'))

// check to see if Hangfire process monitoring is present
assert WebUI.verifyElementPresent(findTestObject('Page_Synect Data Server (3)/a_Hangfire Process Monitoring'), 5) : 'Check to see of the hangfire process monitoring is present'

// check to see if A_S is present
assert WebUI.verifyElementPresent(findTestObject('Page_Synect Data Server (3)/a_Seq Server UI'), 5) : 'Check to see if the a_seq Server UI is present'

// check to see if Hangfire process monitoring is present
assert WebUI.verifyElementPresent(findTestObject('Page_Synect Data Server (3)/a_Swagger API'), 5) : 'Check to see if a_Swagger API is present'

WebUI.waitForElementVisible(findTestObject('Page_Synect Data Server (1)/a_Swagger API'), 3)

WebUI.click(findTestObject('Page_Synect Data Server (1)/a_Swagger API'))

//Checks on swagger logo
assert WebUI.verifyElementPresent(findTestObject('Page_Swagger UI_CreateUser/a_swagger'), 5) : 'Swagger logo not present'

assert WebUI.verifyElementPresent(findTestObject('Page_Swagger UI_CreateUser/div_SynectDataServer'), 5) : 'Swagger logo not present'

WebUI.click(findTestObject('Page_Swagger UI (1)/a_SecurityModuleApi'))

assert WebUI.verifyElementPresent(findTestObject('Page_Swagger UI (2)/a_apiSecurityModuleRegister'), 5) : 'Security Module Register not found'

//WebUI.delay(3)

WebUI.waitForElementVisible(findTestObject('Page_Swagger UI Delete/a_apiSecurityModuleRemoveUser'), 3)

WebUI.click(findTestObject('Page_Swagger UI Delete/a_apiSecurityModuleRemoveUser'))

WebUI.waitForElementVisible(findTestObject('Page_Swagger UI Delete/textarea_credentials'), 3)

WebUI.click(findTestObject('Page_Swagger UI Delete/textarea_credentials'))

String Delname = "\"Diegoo"


WebUI.setText(findTestObject('Page_Swagger UI Delete/textarea_credentials'), '{"Username":' + Delname + '\",')

WebUI.sendKeys(findTestObject('Page_Swagger UI Delete/textarea_credentials'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Page_Swagger UI Delete/textarea_credentials'), '{"Username":' + Delname + '\",\n"Password": "Saaaam",\n')

WebUI.sendKeys(findTestObject('Page_Swagger UI Delete/textarea_credentials'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Page_Swagger UI Delete/textarea_credentials'), '{"Username":' + Delname + '\",\n"Password": "Saaaam",\n"Token": "string"}')

WebUI.waitForElementVisible(findTestObject('Page_Swagger UI Delete/input_submit'), 3)

WebUI.click(findTestObject('Page_Swagger UI Delete/input_submit'))

//////

WebUI.waitForElementVisible(findTestObject('Page_Swagger UI (1)/a_apiSecurityModuleRegister'), 3)

WebUI.click(findTestObject('Page_Swagger UI (1)/a_apiSecurityModuleRegister'))

//WebUI.delay(3)
WebUI.waitForElementVisible(findTestObject('Page_Swagger UI (1)/textarea_credentials'), 3)

assert WebUI.verifyElementPresent(findTestObject('Page_Swagger UI (3)/td_credentials'), 5)

assert WebUI.verifyElementPresent(findTestObject('Page_Swagger UI (3)/input_submit'), 5)

String name = "\"Diegoo"
String quotation = "";
int counter = 0;

WebUI.setText(findTestObject('Page_Swagger UI (1)/textarea_credentials'), '{"Username":' + name + '\",')

WebUI.sendKeys(findTestObject('Page_Swagger UI (1)/textarea_credentials'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Page_Swagger UI (1)/textarea_credentials'), '{"Username":' + name + '\",\n"Password": "Saaaam",\n')

WebUI.sendKeys(findTestObject('Page_Swagger UI (1)/textarea_credentials'), Keys.chord(Keys.ENTER))

WebUI.setText(findTestObject('Page_Swagger UI (1)/textarea_credentials'), '{"Username":' + name + '\",\n"Password": "Saaaam",\n"Token": "string"}')

WebUI.waitForElementVisible(findTestObject('Page_Swagger UI (1)/input_submit'), 3)

WebUI.click(findTestObject('Page_Swagger UI (1)/input_submit'))

assert WebUI.verifyElementPresent(findTestObject('Page_Swagger UI (4)/code_true'), 5) : 'Response was not true'

assert WebUI.verifyElementPresent(findTestObject('Page_Swagger UI (4)/pre_200'), 5) : 'Code was not 200'

//while(findTestObject('Page_Swagger UI (4)/code_false')){
//	System.out.println('Page_Swagger UI (4)/code_false')
//	//String news = CharAt(name.size);
//	counter++;
//	name += (String)counter;
//	WebUI.setText(findTestObject('Page_Swagger UI (1)/textarea_credentials'), '{"Username":' + name + '\",')
//	
//	WebUI.sendKeys(findTestObject('Page_Swagger UI (1)/textarea_credentials'), Keys.chord(Keys.ENTER))
//	
//	WebUI.setText(findTestObject('Page_Swagger UI (1)/textarea_credentials'), '{"Username":' + name + '\",\n"Password": "Saaaam",\n')
//	
//	WebUI.sendKeys(findTestObject('Page_Swagger UI (1)/textarea_credentials'), Keys.chord(Keys.ENTER))
//	
//	WebUI.setText(findTestObject('Page_Swagger UI (1)/textarea_credentials'), '{"Username":' + name + '\",\n"Password": "Saaaam",\n"Token": "string"}')
//	
//	
//	
//	WebUI.waitForElementVisible(findTestObject('Page_Swagger UI (1)/input_submit'), 3)
//	
//	WebUI.click(findTestObject('Page_Swagger UI (1)/input_submit'))
//	
//	
//}

WebUI.delay(5)

WebUI.closeBrowser()

