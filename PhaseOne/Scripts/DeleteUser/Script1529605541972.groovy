import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('http://sds-katalon.azurewebsites.net/Account/Login?ReturnUrl=%2F')

WebUI.setText(findTestObject('DeleteU/Page_Login/input_Email'), 'Admin@synectmedia.com')

WebUI.setText(findTestObject('DeleteU/Page_Login/input_Password'), 'Admin@1234')

WebUI.click(findTestObject('DeleteU/Page_Login/input_loginPage-button'))

WebUI.click(findTestObject('DeleteU/Page_Synect Data Server/a_Dev Tools'))

WebUI.click(findTestObject('DeleteU/Page_Synect Data Server/a_Swagger API'))

WebUI.click(findTestObject('DeleteU/Page_Swagger UI/a_SecurityModuleApi'))
// waits for page to load
WebUI.waitForElementVisible(findTestObject('DeleteU/Page_Swagger UI/a_apiSecurityModuleRemoveUser'), 10)

WebUI.click(findTestObject('DeleteU/Page_Swagger UI/a_apiSecurityModuleRemoveUser'))
WebUI.waitForElementVisible(findTestObject('DeleteU/Page_Swagger UI/textarea_credentials'), 10)

WebUI.setText(findTestObject('DeleteU/Page_Swagger UI/textarea_credentials'), '{\n  "Username": "Diegoo",\n  "Password": "KATALON5567",\n  "Token": "string"\n}')

WebUI.waitForElementVisible(findTestObject('DeleteU/Page_Swagger UI/input_submit'), 10)
WebUI.click(findTestObject('DeleteU/Page_Swagger UI/input_submit'))

//WebUI.closeBrowser()

