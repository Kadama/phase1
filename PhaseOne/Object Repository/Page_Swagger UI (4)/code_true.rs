<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>code_true</name>
   <tag></tag>
   <elementGuidId>9b1db72f-9254-4dcb-ad7e-955b0f06704f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[(text() = 'true' or . = 'true')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>code</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;SecurityModuleApi_SecurityModuleApi_Register_content&quot;)/div[@class=&quot;response&quot;]/div[@class=&quot;block response_body undefined&quot;]/pre[@class=&quot;json&quot;]/code[1]</value>
   </webElementProperties>
</WebElementEntity>
